import React, { Component } from 'react';

import Graph from 'react-graph-vis';

const nodes =
  [
    {id: -1, label: 'Fonte Solta'},
    {id: 0, label: 'Fonte 1', image: 'https://i.ytimg.com/vi/tntOCGkgt98/maxresdefault.jpg', shape: 'circularImage'},
    {id: 1, label: 'Fonte 2', image: 'https://image.flaticon.com/icons/svg/23/23427.svg', shape: 'image', color: 'transparent'},
    {id: 2, label: 'Barra 1', heightConstraint: { minimum: 500 }},
    {id: 3, label: 'Carga 1.1'},
    {id: 4, label: 'Barra 2', heightConstraint: { minimum: 100 }},
    {id: 5, label: 'Barra 3'},
    {id: 6, label: 'Barra 4'},
    {id: 7, label: 'Carga 4.1'},
    {id: 8, label: 'Carga 4.2'},
  ];

const edges =
  [
    {from: -1, to: 6,
      color: {color:'red'},
      dashes: true
    },
    {from: 0, to: 2},
    {from: 1, to: 2},
    {from: 2, to: 3},
    {from: 2, to: 4},
    {from: 4, to: 5},
    {from: 5, to: 6},
    {from: 6, to: 7},
    {from: 6, to: 8},

    // {from: 7, to: 5},
  ];

const options = {
  height: '800px',
  width: '1000px',
  layout: {
    hierarchical: {
      enabled: true,
      sortMethod: 'directed',
      direction: 'LR',
    }
  },
  edges: {
    color: '#000000'
  },
  interaction: {
    dragNodes: false,
    hover: true,
    multiselect: true,
  },
  nodes: {
    shape: 'box'
  },
};

const events = {
  select: function(event) {
    const { nodes, edges } = event;
    console.log(nodes, edges);
  }
};

class App extends Component {
  render() {
    return (
      <Graph graph={{nodes, edges}} options={options} events={events}  />
    );
  }
}

export default App;
